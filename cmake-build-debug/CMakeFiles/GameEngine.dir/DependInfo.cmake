# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "G:/GameEngine/src/main.cpp" "G:/GameEngine/cmake-build-debug/CMakeFiles/GameEngine.dir/src/main.cpp.obj"
  "G:/GameEngine/src/renderer/ShaderProgram.cpp" "G:/GameEngine/cmake-build-debug/CMakeFiles/GameEngine.dir/src/renderer/ShaderProgram.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../external/glfw/include"
  "../external/glad/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "G:/GameEngine/cmake-build-debug/external/glfw/src/CMakeFiles/glfw.dir/DependInfo.cmake"
  "G:/GameEngine/cmake-build-debug/external/glad/CMakeFiles/glad.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
