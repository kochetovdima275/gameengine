#pragma once
#include <iostream>
#include <glad/glad.h>
#include <string>

namespace Renderer{

    class ShaderProgram{
    public:
        ShaderProgram(const std::string& vertexShader, const std::string& fragmentShader);
        ~ShaderProgram();

        [[nodiscard]] bool isCompiled() const;
        void use() const;

        ShaderProgram() = delete;
        ShaderProgram(ShaderProgram&) = delete;
        ShaderProgram& operator = (const ShaderProgram&) = delete;
        ShaderProgram& operator = (ShaderProgram&& shaderProgram) noexcept;
        ShaderProgram(ShaderProgram&& shaderProgram) noexcept;

    private:
        static bool createShader(const std::string& source, GLenum ShaderType, GLuint& shaderID);
        bool m_isCompiled = false;
        GLuint m_ID;

    };
}