#include <iostream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "renderer/ShaderProgram.hpp"

int32_t g_windowSizeX = 640, g_windowSizeY = 480;

GLfloat point[] = {
        0.0f, 0.5f, 0.0f,
        0.5f, -0.5f, 0.0f,
        -0.5f, -0.5f, 0.0f
};

GLfloat color[] = {
        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f
};

const char* vertex_shader =
        "#version 460\n"
        "layout(location = 0) in vec3 vertex_position;"
        "layout(location = 1) in vec3 vertex_color;"
        "out vec3 color;" //? ;
        "void main(){"
        "   color = vertex_color;"
        "   gl_Position = vec4(vertex_position, 1.0);"
        "}";

const char* fragment_shader = //? \n
        "#version 460\n"
        "in vec3 color;"
        "out vec4 frag_color;"
        "void main(){"
        "   frag_color = vec4(color, 1.0);}";

void glfwWindowSizeCallback(GLFWwindow* pWindow, int32_t sizeX, int32_t sizeY){

    g_windowSizeX = sizeX;
    g_windowSizeY = sizeY;

    std::cout << "CHANGING TO: " << sizeX << "\t" << sizeY << std::endl;
    glViewport(0, 0, g_windowSizeX, g_windowSizeY);
}

void glfwKeyCallback(GLFWwindow* pWindow, int32_t key, int32_t scancode, int32_t action, int32_t mode){
    if(action == GLFW_PRESS && key != GLFW_KEY_ESCAPE){

        std::cout << "Key is: " << key << std::endl;

    } else if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS){

        glfwSetWindowShouldClose(pWindow, GL_TRUE);
        std::cout << "Escape pressed" << std::endl;

    }
}

void glfwMouseButtonCallback(GLFWwindow* pWindow, int32_t button, int32_t action, int32_t mode){
    if(action == GLFW_PRESS){
        std::cout << "Mouse key is: " << button << std::endl;
    }
}

int main(){

    try{
        glfwInit();

        //creating hint
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

        //create a window with openGL context
        GLFWwindow* pWindow = glfwCreateWindow(g_windowSizeX, g_windowSizeY, "GameEngine", nullptr, nullptr);

        glfwMakeContextCurrent(pWindow);

        //setting callbacks
        glfwSetWindowSizeCallback(pWindow, glfwWindowSizeCallback);
        glfwSetKeyCallback(pWindow, glfwKeyCallback);
        glfwSetMouseButtonCallback(pWindow, glfwMouseButtonCallback);

        gladLoadGL();

        glClearColor(1, 1, 0, 1);

        //link shaders
        std::string vertexShader(vertex_shader);
        std::string fragmentShader(fragment_shader);

        Renderer::ShaderProgram shaderProgram(vertexShader, fragmentShader);
        if(!shaderProgram.isCompiled()){
            std::cerr << "CAN NOT CREATE SHADER PROGRAM!" << std::endl;
            return -1;
        }

        //create buffers
        GLuint points_vbo = 0;
        glGenBuffers(1, &points_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, points_vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(point), point, GL_STATIC_DRAW);

        GLuint colors_vbo = 0;
        glGenBuffers(1, &colors_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, colors_vbo);
        glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW);

        GLuint vao = 0;
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);

        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, points_vbo);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, colors_vbo);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

        std::cout << glGetString(GL_RENDERER) << std::endl;
        std::cout << glGetString(GL_VERSION) << std::endl;

        while(!glfwWindowShouldClose(pWindow)){
            glClear(GL_COLOR_BUFFER_BIT);

            //drawing triangle
            shaderProgram.use();
            glBindVertexArray(vao);
            glDrawArrays(GL_TRIANGLES, 0, 3);

            glfwSwapBuffers(pWindow);

            glfwPollEvents();
        }

        glfwTerminate();

        std::cout << "Window terminated" << std::endl;

        return 0;

    } catch (std::exception& ex) {

        std::cout << ex.what() << std::endl;

        return -1;
    }
}